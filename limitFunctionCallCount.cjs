module.exports = function limitFunctionCallCount(callBack, counter) {
    // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned

    // PreChecking cb and counter for cornerCases
    if(typeof counter !== 'number') {
        counter = 0;
        console.log('NaN value given, hence default is taken to be 0')
    } 
    if(typeof callBack !== "function") callBack = () => { console.log('CallBack not given') } 
  
    function invokeCallBack () {
        counter >  0 ? callBack(counter--) : console.log(`CallBack Stack reached your provided limit`);
    }
    
    return { invokeCallBack };
}