const cacheFunction = require('../cacheFunction.cjs');

let cb = (n) => { console.log(`CallBack Invoked with ${n} as parameter`) };
const result = cacheFunction(cb);
result.invokeCallBack(3);
result.invokeCallBack('x');
result.invokeCallBack(6);
result.invokeCallBack(3);
result.invokeCallBack('x');
result.invokeCallBack('y');
result.invokeCallBack(6);