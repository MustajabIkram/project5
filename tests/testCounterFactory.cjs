const counterFactory = require('../counterFactory.cjs');

let counterVariable = 65;
const result = [ counterFactory(counterVariable).increment(), counterFactory(counterVariable).decrement() ];
[increment, decrement] = result;
console.log(`Counter variable was ${counterVariable}, 
After increment the result is ${increment}, 
And after decrement the result is ${decrement}`);

let counterVariable1;
const result1 = [ counterFactory(counterVariable1).increment(), counterFactory(counterVariable1).decrement() ];
[increment, decrement] = result1;
console.log(`\nCounter variable was ${counterVariable1}, 
After increment the result is ${increment}, 
And after decrement the result is ${decrement}`);

let counterVariable2 = null;
const result2 = [ counterFactory(counterVariable2).increment(), counterFactory(counterVariable2).decrement() ];
[increment, decrement] = result2;
console.log(`\nCounter variable was ${counterVariable2}, 
After increment the result is ${increment}, 
And after decrement the result is ${decrement}`);

let counterVariable3 = {};
const result3 = [ counterFactory(counterVariable3).increment(), counterFactory(counterVariable3).decrement() ];
[increment, decrement] = result3;
console.log(`\nCounter variable was ${counterVariable3}, 
After increment the result is ${increment}, 
And after decrement the result is ${decrement}`);

let counterVariable4 = [];
const result4 = [ counterFactory(counterVariable4).increment(), counterFactory(counterVariable4).decrement() ];
[increment, decrement] = result4;
console.log(`\nCounter variable was ${counterVariable4}, 
After increment the result is ${increment}, 
And after decrement the result is ${decrement}`);

const result5 = [ counterFactory().increment(), counterFactory().decrement() ];
[increment, decrement] = result5;
console.log(`\nCounter variable was not given, 
After increment the result is ${increment}, 
And after decrement the result is ${decrement}`);