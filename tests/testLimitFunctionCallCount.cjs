const limitFunctionCallCount = require('../limitFunctionCallCount.cjs');

let cb = (n) => { console.log(`CallBack Invoked ${n} times`) };
const result = limitFunctionCallCount(cb, 3);
result.invokeCallBack();
result.invokeCallBack();
result.invokeCallBack();
result.invokeCallBack();
result.invokeCallBack();

console.log('\n')

const result2 = limitFunctionCallCount(cb, );
result2.invokeCallBack();
result2.invokeCallBack();
result2.invokeCallBack();
result2.invokeCallBack();
result2.invokeCallBack();

console.log('\n')

const result3 = limitFunctionCallCount(cb, {});
result3.invokeCallBack();
result3.invokeCallBack();
result3.invokeCallBack();
result3.invokeCallBack();
result3.invokeCallBack();

console.log('\n')

const result4 = limitFunctionCallCount();
result4.invokeCallBack();
result4.invokeCallBack();
result4.invokeCallBack();
result4.invokeCallBack();
result4.invokeCallBack();