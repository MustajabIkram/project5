module.exports = function cacheFunction(callBack) {
    // Should return a function that invokes `callBack`.
    // A cache (object) should be kept in closure scope.
    // The cache should keep track of all arguments have been used to invoke this function.
    // If the returned function is invoked with arguments that it has already seen
    // then it should return the cached result and not invoke `callBack` again.
    // `callBack` should only ever be invokedFunctiond once for a given set of arguments.
    const cache = new Array();

    function invokeCallBack ( param ) {
        if (!cache.includes(param)) {
            console.log(cache);
            cache.push(param);
            return callBack(param);
        } 
        else {
            console.log(cache);
            return cache;
        }
    } 

    return { invokeCallBack };
}